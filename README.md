[0]: https://blog.getpelican.com/
[1]: https://mcss.mosra.cz/

Personal website, powered by [Pelican][0] and the [m.css][1] theme.

# . setup.sh
- Create Virtual Environment
- Install Pelican (pip)
- Install Pelican Plugins (pip) <= Some require further installs

# . acti/deac
- Activate/Deactivate Virtual Environment

# ./update.sh
- InstallPelican (pip)
- Update Pelican and Plugins (some may fail without this)

# . check/serve/hot
- Pelican: Generate HTML (once if *check*)
- Reload on vhsnhrd (if *hot*)

# . publy.sh
- Re-generate Public folder
- git commit
- (optional) git push
