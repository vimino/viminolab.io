#!/bin/bash

#echo "Don't forget to:"
#echo "    git add public"
#echo "    git commit"
#echo "    git push"

# Correct latest git commit message with "git commit --amend"
# Find out how to correct previous git commit messages here:
# https://docs.github.com/en/github/committing-changes-to-your-project/changing-a-commit-message

read -t 10 -p "Remove (yN) ? " yn
case $yn in
    [Yy]* )
        rm public -r
esac

pipenv run pelican ./content/ -s publishconf.py -o public;

mv public/pages/403.html public/;
mv public/pages/404.html public/;

git add public;

git commit;

read -t 10 -p "Push (yN) ? " yn
case $yn in
    [Yy]* )
        git push;
    #break;;
    #[Nn]* ) exit; break;;
    #* ) echo; break;;
esac
