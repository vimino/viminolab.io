#!/bin/sh

# Quick Pelican (SSG) Site Setup
# Version: 6, Date: 2022-01-01
# https://www.shellhacks.com/yes-no-bash-script-prompt-confirmation/
# https://github.com/pypa/pipenv/issues/3890

read -t 10 -p "Clear existing (yN) ? " yn
case $yn in
    [Yy]* )
        pipenv --rm
        rm Pipfile.lock -f
        pipenv install
        pipenv run pip install pelican
        ;;

    * )
        ;;
esac

git init;
git submodule add https://github.com/getpelican/pelican-plugins;

pipenv run pip install Markdown css-html-js-minify
